Explica amb les teves paraules les diferències generals entre la web 1.0, 2.0 i 3.0. Digues alguns exemples de cadascuna.

Explica el paradigma Client-Servidor. Fes servir esquemes i imatges.
    
Cerca informació en Internet sobre els protocols http i https. Explica amb les teves paraules les diferències entre ells. Indica els enllaços d’on has extret la informació.
    
Esbrina la diferència entre llenguatges interpretats en el costat client i llenguatges interpretats en el costat servidor. Fes una llista dels mateixos.
        Llenguatge
        Servidor
        Client

Investiga en Internet i fes una llista amb totes les eines que trobis que permetin publicar un blog o una pàgina web qualsevol. Descriu les seves principals característiques.
        Eina
        Llicència
        Característiques
    
Elabora una llista amb algunes xarxes socials que coneguis i afegeix altres noves cercant informació a Internet. Descriu les seves principals característiques.
        Xarxa social
        Propòsit
        Públic


